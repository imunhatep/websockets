#!/usr/bin/env php
<?php
define('DOCROOT', __DIR__.'/../');
require DOCROOT . '/vendor/autoload.php';

use Inbox\WebSocketBundle\Command\SocketServerCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new SocketServerCommand);
$application->run();