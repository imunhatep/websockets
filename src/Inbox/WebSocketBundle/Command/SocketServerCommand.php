<?php
namespace Inbox\WebSocketBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Inbox\WebSocketBundle\Controller\ChatController;
use Inbox\WebSocketBundle\Service\Scheduler;
use Inbox\WebSocketBundle\Service\Server;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;

class SocketServerCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('inbox:websocket')
            ->setDescription('PHP Websocket server based on coroutineIO')
            ->addArgument(
                'host',
                InputArgument::OPTIONAL,
                'Websocket server hostname'
            )
            ->addArgument(
                'port',
                InputArgument::OPTIONAL,
                'Websocket server port'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $host = $input->getArgument('host');
        $port = $input->getArgument('port');
        if (!$port) {
            $port = '8089';
        }

        if (!$host) {
            $host = 'localhost';
        }

        $eventLoop = new Scheduler();

        $socket = new Server($eventLoop);
        $socket->listen($port, $host);

        $server = new IoServer(new HttpServer(new WsServer(new ChatController())), $socket, $eventLoop);
        //$server = new IoServer(new ChatController(), $socket, $eventLoop);
        $output->writeln('<info>Websocket server starting on: '.$host.':'.$port.'</info>');
        $server->run();
    }
}