<?php
/**
 * Created by PhpStorm.
 * User: aly
 * Date: 3/4/14
 * Time: 1:07 PM
 */

namespace Inbox\WebSocketBundle\Service;


use CoroutineIO\Scheduler\SystemCall;
use CoroutineIO\Scheduler\Task;
use CoroutineIO\Scheduler\Value;
use CoroutineIO\Socket\Socket;
use CoroutineIO\Socket\StreamSocketInterface;
use Evenement\EventEmitterTrait;
use React\Socket\ConnectionInterface;
use React\Stream\Util;
use React\Stream\WritableStreamInterface;

class Connection extends Socket implements ConnectionInterface, StreamSocketInterface
{
    use EventEmitterTrait;

    public $bufferSize = 1500; //typical MTU size
    protected $socket;
    protected $readable = true;
    protected $writable = true;
    protected $closing = false;


    protected $loop;
    protected $buffer;

    function __construct($socket, SocketLoopInterface $loop)
    {
        if(!is_resource($socket)){
            throw new \InvalidArgumentException('Connection constructor expect a valid PHP resource as first arg');
        }

        $this->socket = $socket;
        $this->loop = $loop;
        $this->buffer = new Buffer($this, $this->loop);

        $this->buffer->on(
            'error',
            function ($error) {
                $this->emit('error', array($error, $this));
                $this->close();
            }
        );

        $this->buffer->on(
            'drain',
            function () {
                $this->emit('drain', array($this));
            }
        );
    }

    /**
     * @param bool $block
     */
    function block($block = false)
    {
        stream_set_blocking($this->socket, $block);
    }

    function isReadable()
    {
        return $this->readable;
    }

    function isWritable()
    {
        return $this->writable;
    }

    /**
     * @return static
     */
    function accept()
    {
        yield new SystemCall(function (Task $task, SocketLoopInterface $scheduler) {
            $scheduler->addReader($this, $task);
        }, 's:accept');

        while($this->readable){
            yield new Value(new static(stream_socket_accept($this->socket, 0), $this->loop));
        }
    }

    function pause()
    {
        yield new SystemCall(function (Task $task, SocketLoopInterface $scheduler) {
            $scheduler->removeReadStream($this->socket);
        }, 's:pause');
    }

    function resume()
    {

//        yield new SystemCall(function (Task $task, SocketLoopInterface $scheduler) {
//            $task = new Task($task->getId(), $this->handleData(), 'Resume::handleData');
//            $scheduler->addReader($this, $task);
//        }, 's:resume');

        $task = new Task(0, $this->handleData(), 'Resume::handleData');
        $this->loop->addReader($this, $task);
    }

    function getBuffer()
    {
        return $this->buffer;
    }

    function handleData()
    {
        while ($this->readable) {
//            yield new SystemCall(function (Task $task, SocketLoopInterface $scheduler) {
//                $scheduler->addReader($this, $task);
//            }, 's:handleData');


            yield new SystemCall(function (Task $task, SocketLoopInterface $scheduler) {
                //echo "\n" . 'Handling data stream: ' . (int)$this->socket . "\n";
                $data = stream_socket_recvfrom($this->socket, $this->bufferSize);
                if (feof($this->socket)) {
                    $this->end();
                }
                else {
                    $this->emit('data', array($data, $this));
                }
            }, 's:handleData');

        }
    }

    /**
     * @param $data
     *
     * @return int
     */
    function write($data)
    {
        if ($this->writable) {
            $this->buffer->write($data);
        }

        //$task = new Task(0, $this->handleData(), 'Resume::handleData');
        //yield $this->loop->addReader($this, $task);
//
//        yield new Value(parent::write($data));
    }

    function handleClose()
    {
        if (is_resource($this->socket)) {
            parent::close();
        }
    }

    function close()
    {
        if ($this->writable or $this->closing) {
            $this->closing = false;

            $this->readable = false;
            $this->writable = false;

            $this->emit('end', array($this));
            $this->emit('close', array($this));
            $this->loop->removeStream($this->socket);
            $this->buffer->removeAllListeners();
            $this->removeAllListeners();

            $this->handleClose();
        }
    }

    function end($data = null)
    {
        if ($this->writable) {
            $this->closing = true;

            $this->readable = false;
            $this->writable = false;

            $this->buffer->on(
                'close',
                function () {
                    $this->close();
                }
            );

            $this->buffer->end($data);
        }
    }

    function pipe(WritableStreamInterface $dest, array $options = [])
    {
        Util::pipe($this, $dest, $options);
        yield $dest;
    }

    function getRemoteAddress()
    {
        return $this->parseAddress($this->getRemoteName());
    }

    /**
     * @return string
     */
    function getRemoteName()
    {
        return stream_socket_get_name($this->socket, true);
    }

    /**
     * @return string
     */
    function getLocalName()
    {
        return stream_socket_get_name($this->socket, false);
    }

    protected function parseAddress($address)
    {
        return trim(substr($address, 0, strrpos($address, ':')), '[]');
    }

}