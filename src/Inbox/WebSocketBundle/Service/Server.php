<?php

namespace Inbox\WebSocketBundle\Service;

use CoroutineIO\Exception\Exception;
use CoroutineIO\Scheduler\SystemCall;
use CoroutineIO\Scheduler\Task;
use CoroutineIO\Socket\StreamSocket;
use Evenement\EventEmitter;
use React\EventLoop\LoopInterface;
use React\Socket\ServerInterface;

/**
 * Simple HTTP Server Implementation
 *
 * @author Kazuyuki Hayashi <hayashi@valnur.net>
 *
 * @codeCoverageIgnore
 */
class Server extends EventEmitter implements ServerInterface
{

    private $loop;
    private $server;
    private $isClosed;

    function __construct(SocketLoopInterface $loop)
    {
        $this->isClosed = false;
        $this->loop = $loop;
    }

    /**
     * @param $port
     * @param string $host
     * @return resource
     * @throws \CoroutineIO\Exception\Exception
     */
    function listen($port, $host = 'localhost')
    {
        $this->loop->add($this->createSocket($port, $host), 'Listen server');
    }

    /**
     * @param $port
     * @param $host
     * @return \Generator
     * @throws \CoroutineIO\Exception\Exception
     */
    function createSocket($port, $host)
    {
        $this->server = stream_socket_server('tcp://' . $host . ':' . $port, $no, $str);
        if (!$this->server) {
            throw new Exception("$str ($no)");
        }

        $server = new Connection($this->server, $this->loop);
        $server->block(false);

        yield $this->accept($server);

//      while (!$this->isClosed) {
//            yield SystemCall::create($this->accept($server)
//                $this->handleClient(yield $server->accept()), 'Server::handleClient'
//            );
        //}

        //$server->close();
    }


    /**
     * @param Connection $server
     * @return \Generator
     */
    protected function accept(Connection $server)
    {
        yield new SystemCall(function (Task $task, SocketLoopInterface $scheduler) use ($server){
            $scheduler->addReader($server, $task);
        }, 's:accept');

        while (!$this->isClosed) {
            yield new SystemCall(function (Task $task, SocketLoopInterface $scheduler) use ($server){
                $this->handleClient(new Connection(stream_socket_accept($server->getRaw(), 0), $this->loop));
            });
        }

        $server->close();
    }

    function handleClient(Connection $client)
    {
        $client->block(false);

        /** @var Connection $client */
        $this->emit('connection', array($client));
        $client->resume();
    }

    function getPort()
    {
        $name = stream_socket_get_name($this->server, false);

        return (int)substr(strrchr($name, ':'), 1);
    }

    function shutdown()
    {
        $this->isClosed = true;
        $this->loop->removeStream($this->server);
        $this->removeAllListeners();
    }
}