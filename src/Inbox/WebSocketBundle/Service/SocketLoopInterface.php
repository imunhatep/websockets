<?php
/**
 * Created by PhpStorm.
 * User: aly
 * Date: 3/5/14
 * Time: 11:45 AM
 */

namespace Inbox\WebSocketBundle\Service;


use CoroutineIO\Scheduler\Task;
use CoroutineIO\Socket\SocketSchedulerInterface;
use React\EventLoop\LoopInterface;

interface SocketLoopInterface extends SocketSchedulerInterface, LoopInterface
{
    /**
     * {@inheritdoc}
     */
    function add(\Generator $coroutine, $name = '');

    /**
     * {@inheritdoc}
     */
    function addReadStream($stream, callable $listener);

    /**
     * {@inheritdoc}
     */
    function addWriteStream($stream, callable $listener);

    /**
     * {@inheritdoc}
     */
    function removeReadStream($stream);

    /**
     * {@inheritdoc}
     */
    function removeWriteStream($stream);

    /**
     * {@inheritdoc}
     */
    function removeStream($stream);
}