<html>
<head>
    <style>
        #output {
            padding: .5em;
            border-left: 1px #ccc solid;
            width: 49%;
            float: right;
        }

        h3 {
            padding: 0;
            margin: .5em;
        }

        input {
            width: 50em;
        }

        .me {
            color: #226622;
        }

        .other {
            color: #aa5500;
        }
    </style>
</head>
<body>
<div id="output"></div>

<input type="text" id="message" value="" placeholder="sms"/>
<button name="send" id="send">&crarr;</button>

<script type="text/javascript">
    var outputDiv = document.getElementById('output');
    var button = document.getElementById('send');
    var message = document.getElementById('message');

    var conn = new WebSocket('ws://172.16.10.133:8081');
    conn.onopen = function (e) {
        outputDiv.innerHTML ='<h3>Connected</h3>';
    }

    conn.onclose = function(e){
        outputDiv.innerHTML ='<h3>Disconnected</h3>';
        console.log(e);
    }

    conn.onmessage = function (e) {
        outputDiv.innerHTML = [
            outputDiv.innerHTML, '<br>',
            '<span class="other">sender: ', e.data, '</span>'
        ].join('');
    };

    message.onkeydown = function(e){
        if(e.keyCode == 13){
            e.preventDefault();

            var data = message.value;
            message.value = "";

            outputDiv.innerHTML = [
                outputDiv.innerHTML, '<br>',
                '<span class="me">me: ', data, '</span>'
            ].join('');

            conn.send(data);
        }
    }

    button.onclick = function(e){
        var data = message.value;
        message.value = "";

        conn.send(data);
    }
</script>
</body>
</html>
<?php

