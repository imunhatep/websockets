<html>
<head>
    <style>
        #output {
            padding: .5em;
            border-left: 1px #ccc solid;
            width: 49%;
            float: right;
        }

        h3 {
            padding: 0;
            margin: .5em;
        }

        input {
            width: 50em;
        }

        .me {
            color: #226622;
        }

        .other {
            color: #aa5500;
        }
    </style>
</head>
<body>
<div style="float: left">
    <button name="start" id="start">start</button><br>
    <button name="stop" id="stop">stop</button><br>
</div>
<div id="output"></div>

<script type="text/javascript">
    var outputDiv = document.getElementById('output'),
        startButton = document.getElementById('start'),
        stopButton = document.getElementById('stop'),
        stopConnecting = false,
        startTime = Date.now(),
        conns = [],
        successConns = 0,
        errorConns = 0,
        closedConns = 0;


    function WsConnection(url)
    {
        var conn = new WebSocket(url);
        conn.onopen = this.onopen;
        conn.oclose = this.onclose;
        conn.onerror = this.onerror;
    }

    WsConnection.prototype = {
        lastErrorMsg: '',
        getSocket: function(){ return conn; },
        onopen: function (e) { successConns++;},
        onclose: function (e) { closeConns++;},
        onerror: function (e) {

            if(this.lastErrorMsg != e){
                this.lastErrorMsg = e;
                console.log(e);
            }

            errorConns++;
        }
    }

    function makeConnection(i){
        if(i++ > 10000){
            return;
        }

        conns.push(new WsConnection('ws://localhost:8081'));

        outputDiv.innerHTML =
            '<p>Success: '+ successConns +'</p>' +
            '<p>Closed: '+ closedConns +'</p>' +
            '<p>Errors: '+ errorConns +'</p>';

        if(errorConns < 50 && !stopConnecting ){
            setTimeout(function(){ makeConnection(i); } , 100);
        }
    }

    startButton.onclick = function(e){
        stopConnecting = false;
        this.disabled = "disabled";
        makeConnection(0);
    }

    stopButton.onclick = function(e){
        stopConnecting = true;
        startButton.removeAttribute("disabled");
    }
</script>

</body>
</html>
<?php

